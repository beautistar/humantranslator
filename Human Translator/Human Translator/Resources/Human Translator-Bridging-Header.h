//
//  Human-Translator-Bridging-Header.h
//  Human Translator
//
//  Created by Yin on 09/02/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

#ifndef Human_Translator_Bridging_Header_h
#define Human_Translator_Bridging_Header_h

#import "UIImage+ResizeMagick.h"
#import "RTCPeerConnectionFactory.h"
#import "RTCEAGLVideoView.h"
#import "ARDAppClient.h"


#endif /* Human_Translator_Bridging_Header_h */
