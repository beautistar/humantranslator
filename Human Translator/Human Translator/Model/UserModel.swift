//
//  UserModel.swift
//  Human Translator
//
//  Created by Yin on 09/02/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

import Foundation

class UserModel {
    
    var id = 0
    var name = ""
    var email = ""
    var password = ""
    var user_type = 0
    var gender = 0
    var age = 10
    var photo_url = ""
    var my_language = ""
    var trans_language = ""
    var about_me = ""
    var is_available = 0
    var avg_mark: Float = 0.0    
}
