//
//  LanguageModel.swift
//  Human Translator
//
//  Created by Yin on 09/02/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

import Foundation

class LanguageModel {
    
    var code = ""
    var name = ""
    var nativeName = ""
}
